var employees = [
    { "internalid": "1", "name": "Abe Anderson", "email": "aanderson@javascript.com", "birthdate": "9/25/1974", "supervisor": "3", "2012 Revenue": "100000.00", "2013 Revenue": "0.00" },
    { "internalid": "2", "name": "Bob Benson", "email": "bbenson@javascript.com", "birthdate": "7/13/1972", "supervisor": "3", "2012 Revenue": "150000.00", "2013 Revenue": "0.00" },
    { "internalid": "3", "name": "Chelsea Chastain", "email": "cchastain@javascript.com", "birthdate": "5/7/1968", "supervisor": "", "2012 Revenue": "375000.00", "2013 Revenue": "0.00" },
    { "internalid": "4", "name": "Dwight Dwyer", "email": "ddwyer@javascript.com", "birthdate": "8/23/1982", "supervisor": "3", "2012 Revenue": "125000.00", "2013 Revenue": "0.00" },
    { "internalid": "5", "name": "Eathon Eckhart", "email": "eeckhart@javascript.com", "birthdate": "11/28/1970", "supervisor": "", "2012 Revenue": "200000.00", "2013 Revenue": "0.00" }
];

var revenue2013 = [
    { "type": "invoice", "customer": "Franklin", "Employee": "1", "amount": "50000.00" },
    { "type": "invoice", "customer": "Gabby", "Employee": "1", "amount": "25000.00" },
    { "type": "invoice", "customer": "Harry", "Employee": "1", "amount": "30000.00" },
    { "type": "invoice", "customer": "Ingrid", "Employee": "2", "amount": "75000.00" },
    { "type": "invoice", "customer": "Jacob", "Employee": "2", "amount": "60000.00" },
    { "type": "invoice", "customer": "Kelly", "Employee": "4", "amount": "30000.00" },
    { "type": "invoice", "customer": "Lamar", "Employee": "4", "amount": "40000.00" },
    { "type": "invoice", "customer": "Mary", "Employee": "4", "amount": "20000.00" },
    { "type": "invoice", "customer": "Nicole", "Employee": "4", "amount": "70000.00" },
    { "type": "invoice", "customer": "Oscar", "Employee": "5", "amount": "75000.00" },
    { "type": "invoice", "customer": "Patrick", "Employee": "5", "amount": "80000.00" },
    { "type": "invoice", "customer": "Quin", "Employee": "5", "amount": "60000.00" },
    { "type": "invoice", "customer": "Rachel", "Employee": "5", "amount": "100000.00" }
];

var commissionRules = [
    { "employee": "1", "percentage": "15%", "bonus": "2000.00" },
    { "employee": "2", "percentage": "10%", "bonus": "3000.00" },
    { "employee": "3", "percentage": "7.5%", "bonus": "5000.00" },
    { "employee": "4", "percentage": "10%", "bonus": "3000.00" },
    { "employee": "5", "percentage": "10%", "bonus": "3000.00" }
];

// function to calculate and create days to birthday table
const dayToBirth=()=>{
    let today_ = new Date("1/1/2014") // create date object for today date 1/1/2014
    employees.map(emp=>{ // loop over every object of the employees array
        let bd = new Date(emp.birthdate), //birth day
        bm = bd.getMonth(), //birth month
        bdate = bd.getDate(), //birth date
            next_birth_day = new Date(bm + "/" + bdate + "/" + today_.getFullYear());
        emp['day_to_birthday'] = new Date(next_birth_day - today_) / (1000 * 60 * 60 * 24) //convert milliseconds to secs=>mins=>hours=>days
    })
    tables = `
<tr>
    <th colspan="10">employees</th>
</tr>
<tr>
    <th>ID</th>
    <th>NAME</th>
    <th>EMAIL</th>
    <th>BIRTHDAY</th>
    <th>SUPERVISOR</th>
    <th>2012 Revenue</th>
    <th>2013 Revenue</th>
    <th>DAYS TO NEXT B.D</th>
</tr>
    `
    employees.map(emp=>{
    tables+=`
    <tr>
        <td>${emp.internalid}</td>
        <td>${emp.name}</td>
        <td>${emp.email}</td>
        <td>${emp.birthdate}</td>
        <td>${emp.supervisor}</td>
        <td>${emp['2012 Revenue']}</td>
        <td>${emp['2013 Revenue']}</td>
        <td>${emp.day_to_birthday}</td>
    </tr>`
    })
    document.getElementById('tbdy').innerHTML =tables // WRITE THE TABLE TO html page
}

// find best customer
const findBestCustomer =()=>{
    tbl = `
<tr>
    <th colspan="10">employees</th>
</tr>
<tr>
    <th>ID</th>
    <th>NAME</th>
    <th>EMAIL</th>
    <th>BST_CUSTOMER 2013</th>
    <th>BEST AMOUNT 2013</th>
</tr>
`
    employees.map(emp=>{
        record = _.filter(revenue2013, { Employee: emp.internalid}) // link every revenue item to employee id
        rev = 0
        if(record.length){
            record.map(rec=>{
                if (parseFloat(rec.amount) > rev){
                    rev = parseFloat(rec.amount)
                    emp['best_customer_object'] = rec // add the customer best record to employee
                    emp['best_customer_2013'] = rec.customer // customer name to employee
                    emp['best_amount_2013'] = rec.amount
                }
            })
        }

        
    })
    employees.map(emp=>{
        tbl+=`
<tr>
    <td>${emp.internalid}</td>
    <td>${emp.name}</td>
    <td>${emp.email}</td>
    <td>${emp.best_customer_2013}</td>
    <td>${emp.best_amount_2013}</td>
</tr>`
    })
    document.getElementById('tbdy1').innerHTML = tbl // WRITE THE TABLE TO html page
}

const calculate2013Rev=()=>{

    // calculate revenue for each employee
    employees.map(emp => {
        record = _.filter(revenue2013, { Employee: emp.internalid })
        rev = 0 // initialize initial revenue for 2013
        if (record.length) {
            record.map(rec => {// loop over all revenue records that belong to employee
                rev += parseFloat(rec.amount)  // keep adding employee revenue
            })
        }
        emp['2013 Revenue'] = rev  // add final value to 2013 revenue for the employee

    })
    // get all employees that are supervised by another employee and add all their revenues to the supervisor revenue
    employees.map(emp => {
        record = _.filter(employees, { supervisor: emp.internalid }) 
        sup_amount = 0 // initialize initial revenue for 2013
        if(record.length){ // check that the employee supervised some employees (at least 1)
            record.map(rec=>{ // loop over all revenue records that belong to employee
                sup_amount += parseFloat(rec['2013 Revenue']) // keep adding employee revenue
            })
            emp['2013 Revenue'] = sup_amount // add final value to 2013 revenue for the employee
        }
    })
    
    tables = `
<tr>
    <th colspan="10">employees</th>
</tr>
<tr>
    <th>ID</th>
    <th>NAME</th>
    <th>EMAIL</th>
    <th>BIRTHDAY</th>
    <th>SUPERVISOR</th>
    <th>2012 Revenue</th>
    <th>2013 Revenue</th>
</tr>
    `
    employees.map(emp => {
        tables += `
    <tr>
        <td>${emp.internalid}</td>
        <td>${emp.name}</td>
        <td>${emp.email}</td>
        <td>${emp.birthdate}</td>
        <td>${emp.supervisor}</td>
        <td>${emp['2012 Revenue']}</td>
        <td>${emp['2013 Revenue']}</td>
    </tr>`
    })
    document.getElementById('tbdy2').innerHTML = tables // WRITE THE TABLE TO html page
    
}

const calcCommission =()=>{
    employees.map(emp => {
        comm = _.findWhere(commissionRules, { employee: emp.internalid})
        if(parseFloat( emp['2012 Revenue'])< parseFloat(emp['2013 Revenue'])){ // check that there is increase in revenue
            emp['earned_bonus'] = comm.bonus // add value to bonus key in employee record
        }else{
            emp['earned_bonus'] = 0  // add 0 to bonus key in employee record
        }
        emp['earned_comm'] = (parseFloat(comm.percentage) / 100) * parseFloat(emp['2013 Revenue']) // calculate earned commissions
    })
    tables = `
<tr>
    <th colspan="10">employees</th>
</tr>
<tr>
    <th>ID</th>
    <th>NAME</th>
    <th>EMAIL</th>
    <th>BIRTHDAY</th>
    <th>SUPERVISOR</th>
    <th>2012 Revenue</th>
    <th>2013 Revenue</th>
    <th>COMMISSION</th>
    <th>BONUS</th>
</tr>
    `
    employees.map(emp => {
        tables += `
    <tr>
        <td>${emp.internalid}</td>
        <td>${emp.name}</td>
        <td>${emp.email}</td>
        <td>${emp.birthdate}</td>
        <td>${emp.supervisor}</td>
        <td>${emp['2012 Revenue']}</td>
        <td>${emp['2013 Revenue']}</td>
        <td>${emp['earned_comm']}</td>
        <td>${emp['earned_bonus']}</td>
    </tr>`
    })
    document.getElementById('tbdy3').innerHTML = tables // WRITE THE TABLE TO html page
}

// calculate percentage increase in revenue for each employee

const percentageIncrease =()=>{
    employees.map(emp => {
        emp['% increase'] = ((parseFloat(emp['2013 Revenue']) - parseFloat(emp['2012 Revenue'])) * 100) / parseFloat(emp['2012 Revenue']) // calculate revenue % change
    })
    tables = `
<tr>
    <th colspan="10">employees</th>
</tr>
<tr>
    <th>ID</th>
    <th>NAME</th>
    <th>EMAIL</th>
    <th>BIRTHDAY</th>
    <th>SUPERVISOR</th>
    <th>2012 Revenue</th>
    <th>2013 Revenue</th>
    <th>COMMISSION</th>
    <th>BONUS</th>
    <th>% CHANGE REVENUE</th>
</tr>
    `
    employees.map(emp => {
        tables += `
    <tr>
        <td>${emp.internalid}</td>
        <td>${emp.name}</td>
        <td>${emp.email}</td>
        <td>${emp.birthdate}</td>
        <td>${emp.supervisor}</td>
        <td>${emp['2012 Revenue']}</td>
        <td>${emp['2013 Revenue']}</td>
        <td>${emp['earned_comm']}</td>
        <td>${emp['earned_bonus']}</td>
        <td>${emp['% increase']}</td>
    </tr>`
    })
    document.getElementById('tbdy4').innerHTML = tables // WRITE THE TABLE TO html page
}

// CALCULATE DIFFERENCE BETWEEN HIGHEST AND LOWEST REVENUE FROM CUSTOMERS FOR EACH EMPLOYEE IN 2013

const DifferenceHightLow =()=>{
    employees.map(emp => {
        record = _.filter(revenue2013, { Employee: emp.internalid }) // find all revenues belonging to employee
        lowest = 0
        highest = 0
        if (record.length) {
            record.map((rec,ind) => {
                if(ind===0){
                    lowest = rec.amount // initialize lowest to the first revenue encountered
                    highest = rec.amount // initialize highest to the first revenue encountered
                }else{
                    if (parseFloat(rec.amount) > highest) { // check if amount encountered is higher than highest initialized
                        highest = parseFloat(rec.amount)
                    }
                    if (parseFloat(rec.amount) < lowest) { // check if amount encountered is lower than lowers initialized
                        highest = parseFloat(rec.amount)
                    }
                }
                
            })
        }
        emp['diff_low_high'] = highest =lowest
    })
    tables = `
<tr>
    <th colspan="10">employees</th>
</tr>
<tr>
    <th>ID</th>
    <th>NAME</th>
    <th>EMAIL</th>
    <th>BIRTHDAY</th>
    <th>SUPERVISOR</th>
    <th>2012 Revenue</th>
    <th>2013 Revenue</th>
    <th>COMMISSION</th>
    <th>BONUS</th>
    <th>diff lowest highest</th>
</tr>
    `
    employees.map(emp => {
        tables += `
    <tr>
        <td>${emp.internalid}</td>
        <td>${emp.name}</td>
        <td>${emp.email}</td>
        <td>${emp.birthdate}</td>
        <td>${emp.supervisor}</td>
        <td>${emp['2012 Revenue']}</td>
        <td>${emp['2013 Revenue']}</td>
        <td>${emp['earned_comm']}</td>
        <td>${emp['earned_bonus']}</td>
        <td>${emp['diff_low_high']}</td>
    </tr>`
    })
    document.getElementById('tbdy5').innerHTML = tables // WRITE THE TABLE TO html page

}